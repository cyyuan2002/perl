#!/usr/bin/perl

#===============================================================================
#
#         FILE: 
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Yuan Chen
#      COMPANY: Division of Infectious Disease, DUMC
#      VERSION: 1.0
#      CREATED: 
#     REVISION:
#===============================================================================

use strict;
use Getopt::Long;

sub Usage #help subprogram
{
    print << "    Usage";

	Usage: $0 [options] -i <vcf_file>

		-w            Window Size: default 5000

		-s            Step Size: default 1000
		
		-f            Output Format: 1-circos, 2-R, default 1 

    Usage

	exit(0);
};

my %opts;

GetOptions(\%opts,"i=s","c=s","f:i","w:i","s:i");
if((!defined($opts{i}))){
    Usage();
}

my $InputFile=$opts{'i'};
my $WindowSize=$opts{'w'};
my $StepSize=$opts{'s'};
my $OutFormat=$opts{'f'};

$WindowSize ||= 5000;
$StepSize ||= 1000;
$OutFormat ||= 1;


my %Chrom_length;
my @sites;
my $lastchrom;
my %chrominfo;
open(my $fh_infile,"$InputFile") or die "Can't open file: $InputFile";
while (<$fh_infile>) {
    if(/^#/){
	if (/^##contig=<ID=(\S+),length=(\d+)/) {
	    $Chrom_length{$1}=$2;
	}
	next;
    }
    chomp();
    my @lines=split(/\t/,$_);
    if ($lines[0] ne $lastchrom) {
	if ($lastchrom ne "") {
	    my @tmpsites=@sites;
	    $chrominfo{$lastchrom}=\@tmpsites;
	}
	$lastchrom = $lines[0];
	@sites=();
    }
    push(@sites,$lines[1]);
}
{
    my @tmpsites=@sites;
    $chrominfo{$lastchrom}=\@tmpsites;
}
close $fh_infile;

foreach my $chr(sort keys %Chrom_length){
    my $laststart=0;
    for(my $i=1; ($i + $WindowSize - $StepSize) <= $Chrom_length{$chr};$i+=$StepSize){
	my $SNPcount=0;
	my @SNPsites=@{$chrominfo{$chr}};
        my $isstart=0;
	for(my $j=$laststart;$j<@SNPsites;$j++){
	    if ($SNPsites[$j] >= $i && $SNPsites[$j] <= $i+$WindowSize - 1) {
                if ($isstart==0) {
                    $laststart=$j;
                    $isstart=1;
                }
		$SNPcount++;
	    }
	    last if ($SNPsites[$j] > $i+$WindowSize - 1);   
	}
	if ($OutFormat == 1) {
	    print $chr,"\t",$i,"\t",$i+$StepSize-1,"\t",$SNPcount,"\n" if($SNPcount>0);
	}
	else{
	    print $chr," ",$i," ",$i+$StepSize-1," ",$SNPcount,"\n" if($SNPcount>0);
	}
    }
}