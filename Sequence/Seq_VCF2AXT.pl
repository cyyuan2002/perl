#!/usr/bin/perl
use strict;
use Getopt::Long;
use Bio::DB::Fasta;


sub Usage #help subprogram
{
    print << "    Usage";

	Usage: $0 [options] --vcf <vcf annotation file> --gff <gff file> -g <genome file> -o <output file>

    Usage

	exit(0);
};

sub RepSNP{
    my ($refseq,$chr,$start,$end,$snps)=@_;
    my %chrsnps=%{$snps->{$chr}};
    my @newseqarray=split("",$refseq);
    foreach my $site(sort {$a<=>$b} keys %chrsnps){
	if ($site >= $start && $site <= $end) {
	    my $rel_site=$site-$start;
	    my $nu=$chrsnps{$site};
	    $newseqarray[$rel_site]=$nu;
	}
    }
    my $newseq=join("",@newseqarray);
    return $newseq;
}


my ($vcffile,$gfffile,$refgenome,$outfile);

GetOptions('vcf=s' => \$vcffile,'gff=s' => \$gfffile,'g=s' => \$refgenome, 'o=s' => \$outfile);
if ($vcffile eq "" || $gfffile eq "" || $refgenome eq "" || $outfile eq "") {
    Usage();
}

my $db = Bio::DB::Fasta->new($refgenome);
$| = 1; 
print STDERR ("Genome fasta parsed\n");


my $snps;
open(my $fh_vcf,"$vcffile") or die "Can't open file: $vcffile\n";
while (<$fh_vcf>) {
    chomp();
    next if(/^#/);
    my @lines=split(/\t/,$_);
    $snps->{$lines[0]}->{$lines[1]}=$lines[4];
}
close $fh_vcf;

my %CDS;
my %Genes;
my $mRNA_name;
my $frame;

open(my $fh_gff,"$gfffile") or die "Can't open file: $gfffile\n";
while (<$fh_gff>) {
    chomp();
    my @lines=split(/\t/,$_);
    my $type=$lines[2];
    
    if ($type eq 'gene') {
        my @attrs = split( ";", $lines[8]);
        $attrs[0] =~ s/ID=//;
        my $gene_name = $attrs[0];
        my $gene_chr=$lines[0];
        my $gene_start = $lines[3];
        my $gene_end = $lines[4];
        my $gene_seq = $db->seq( $lines[0], $gene_start, $gene_end );
        my $snp_seq=RepSNP($gene_seq,$gene_chr,$gene_start,$gene_end,$snps);
	$Genes{$gene_name}->{'refseq'}=$gene_seq;
	$Genes{$gene_name}->{'snpseq'}=$snp_seq;
	$Genes{$gene_name}->{'start'}=$gene_start;
	$Genes{$gene_name}->{'end'}=$gene_end;
	$Genes{$gene_name}->{'chr'}=$gene_chr;
	$Genes{$gene_name}->{'dir'}=$lines[6];
    }
    
    if ( $type eq 'CDS' ) {
	my @attrs = split( ";", $lines[8]);
        my $cds_coord = $lines[3] . " " . $lines[4];
	$attrs[1] =~ s/Parent=//;
        $CDS{$attrs[1]}->{$lines[3]}=$cds_coord;
    }
}
close $fh_gff;

my %genenames;
open(my $fh_out,">","$outfile");
foreach my $trans(keys %CDS){
    my %exons=%{$CDS{$trans}};
    my $mergedCDS_seq;
    my $mergedSNP_seq;
    $trans=~/(\S+)T\d+/;
    my $gene=$1;
    if (exists($genenames{$gene})) {
	next;
    }
    else{
	$genenames{$gene}=1;
    }
    foreach my $key (sort {$a <=> $b} keys %exons) { # Ascending numeric sort of the starting coordinate
        my $coord = $exons{$key};
        my @cds_coord = split( " ", $coord );
	my $length=$cds_coord[1]-$cds_coord[0]+1;
	my $gene_start=$Genes{$gene}->{'start'};
	my $rel_start=$cds_coord[0]-$gene_start;
	my $refseq=$Genes{$gene}->{'refseq'};
	my $snpseq=$Genes{$gene}->{'snpseq'};
        $mergedCDS_seq .= substr($refseq,$rel_start,$length);
	$mergedSNP_seq .= substr($snpseq,$rel_start,$length);
    }
    if ($Genes{$gene}->{'dir'} eq "-") {
	$mergedCDS_seq =~ tr/ACGTacgt/TGCAtgca/;
	$mergedCDS_seq = reverse ($mergedCDS_seq);
	$mergedSNP_seq =~ tr/ACGTacgt/TGCAtgca/;
	$mergedSNP_seq = reverse ($mergedSNP_seq);
    }
    print $fh_out "$gene\n$mergedCDS_seq\n$mergedSNP_seq\n\n";
    
}
close $fh_out;

exit(0);