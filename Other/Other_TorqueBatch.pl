#!/usr/bin/perl
use strict;
use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use File::Temp qw/ tempfile tempdir /;
my %opts;
GetOptions(\%opts,"i:s","r:s","e:i","q:s","m:s","p:s","a:s","help");

if(!defined $opts{i}){
	&Usage();
}

my $fileName;
my $pbsName;
my $ppNumber;
my $pbsFile;
my $directory;
my $emailnotify;
my $commandFile;
my $queueName;
my $addition;
my $memSize;

my $scriptdir = dirname(abs_path($0));
my $Torquecommand = $scriptdir."/Other_TorqueCommand.pl";

if (! -e $Torquecommand) {
	die "Cannot find $Torquecommand";
}

$commandFile=$opts{i};
$addition=$opts{a};
$directory=$opts{r};
$ppNumber=$opts{p};
$emailnotify=(defined $opts{e}) ? $opts{e} : 0;
$queueName=$opts{q};
$memSize=$opts{m};

my $commandcount = 0;
open(my $fh, "$commandFile") or die "Cannot open file:$commandFile";
while (<$fh>) {
	chomp();
	$commandcount ++;
	my @commands;
	my $shcommand;
	@commands = split("\t",$_);
	if (scalar(@commands) > 1) {
		$pbsName = $commands[1];
		$shcommand = $commands[0]
	}
	else{
		$pbsName = "$commandFile.Job$commandcount ";
		$shcommand = $_
	}
	my $command = "";
	$command = " -c \"$shcommand\" -n $pbsName ";
	if ($directory ne "") {
		$command .= "-r $directory ";
	}
	if ($ppNumber ne "") {
		$command .= "-p $ppNumber ";
	}
	if ($addition ne "") {
		$command .= "-a $addition ";
	}
	if ($emailnotify ne "") {
		$command .= "-e $emailnotify ";
	}
	if ($queueName ne "") {
		$command .= "-q $queueName ";
	}
	if ($memSize ne "") {
		$command .= "-m $memSize ";
	}
	`perl $Torquecommand $command`;
}
close($fh);


sub Usage(){
	print << "    Usage";

	Version	  : 1.0
	Date	  : 2014-11-5
	Author    : Yuan,Chen
        Email     : ychen\@aperiomics.com

	Usage: $0 <options>

		-i     command file, must be given (String)

		-r     job directory , default: current dir(String)

		-a     additional parameters for TorquePBS

		-p     number of processes for each job, default value is 1

		-e     email notification when job finished, 0 None, 1 to Yuan, 2 to Cory (default 0)

		-q     job queue, default: batch
		
		-m     Memory size required

		-help  Show help

    Usage

	exit(0);
}
