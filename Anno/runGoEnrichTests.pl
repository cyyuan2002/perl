#!/usr/bin/env perl
#
#
# enrichment test script
# copied from ~cdesjard/plscripts/mol_evol_tools/b2gfisherX.pl
# questions email cdesjard@broadinstitute.org

use strict;
use warnings;
use Data::Dumper;
use Getopt::Long qw(:config no_ignore_case bundling pass_through);
use Statistics::Multtest qw(BH qvalue);
use FindBin;

use lib ("$FindBin::Bin/lib");

use Stats::WilsonInterval;
use Stats::RightFisher;
use Stats::TwoFisher;

my $usage = <<__EOUSAGE__;

################################################################################################ 
#
#  Conducts two-tailed Fisher's exact test with q-value FDR between files with ;-separated GO terms aware of GO DAG
#
#  Mandatory:
#
#  --annot_file_1 <string>         tab-delimited text file: ids in first column, ;-separated terms in second
#                                   ex.
#                                        locus_01	GO:00001;GO:00002
#                                        locus_02	GO:00001
#                                        locus_03	GO:00002;GO:00004
#
#  --annot_file_2 <string>         tab-delimited text file: ids in first column, ;-separated terms in second
#                                   ex.
#                                        locus_04	GO:00001
#                                        locus_05	GO:00001;GO:00002
#                                        locus_06	GO:00003;GO:00004
#
#  Optional:
#
#  --right_tailed                  conducts right-tailed Fisher test instead of two-tailed
#
#  --term_col                      tab-delimited column # where term appears (default = 1)
#
#  --obo_file                      path to custom obo file
#
#  Notes:
#
#    Ignores term lines coded as "." as appear in the cobra data repository files to denote empty values
#    Q-values take a while to compute - blame perl, not me!
#
################################################################################################

__EOUSAGE__

    ;

my $b1file;
my $b2file;
my $right_flag;
my $term_col = 1;
my $obo_file = "$FindBin::Bin/lib/gene_ontology.1_2.obo.txt";

&GetOptions (
    'annot_file_1=s' => \$b1file,
    'annot_file_2=s' => \$b2file, 
    'term_col=i' => \$term_col,
    'right_tailed' => \$right_flag,
    'obo_file=s' => \$obo_file,
    );

if (@ARGV) {
    die "Error, don't understand parameters: @ARGV";
}

unless ($b1file and $b2file) {
    die $usage;
}

##### Read in Files #####

unless (open (OBOFILE, $obo_file)) {                # read in obo file
    die "File $obo_file not found.\n";
}

my %go_desc;
my %has_parents;
my $current_go;

foreach my $in (<OBOFILE>) {
    chomp $in;
    if ($in =~ /^id: (GO:\d+)/) {
        $current_go = $1;
    } elsif ($in =~ /^name: ([\S ]+)/) {
        $go_desc{$current_go} = $1;
    } elsif ($in =~ /^is_a: (GO:\d+)/) {
        push (@{$has_parents{$current_go}}, $1);
    } elsif ($in =~ /^relationship: part_of (GO:\d+)/) {
        push (@{$has_parents{$current_go}}, $1);
    }
}
close OBOFILE;

#print Dumper(\%has_parents);

#exit;

unless (open (B1FILE, $b1file)) {                # read in b2g files
    die "File $b1file not found.\n";
}

my %go_counts1;
my %genes_1;
my %go_terms;
my %gene2go_1; #####
my $sp1_gc = 0; 

foreach my $in (<B1FILE>) {
    chomp $in;
    my @x = split (/\t/, $in);
    if ($x[$term_col] ne '.' and $x[0] !~ /^#/) {
        ++$sp1_gc;
        $genes_1{$x[0]} = 1;

        my @y = split (/;/, $x[$term_col]);
        foreach my $y (@y) {
            my $current_go = $y;
            ### print "$current_go"; ###
            my @current_parents;
            my @family;
            push @family, $current_go;
            if ($has_parents{$current_go}) {  					##### working area
                @current_parents = @{$has_parents{$current_go}};
            } elsif ($current_go eq 'GO:0008150' or $current_go eq 'GO:0005575' or $current_go eq 'GO:0003674') {
            } else {
                print STDERR "No parents found for $current_go. Term may be obsolete or have an alternate id.\n"
            }
    
            while (@current_parents) {
                $current_go = shift @current_parents;
                push @family, $current_go;
                ### print " $current_go"; ###
        
                if ($has_parents{$current_go}) {
                    my @new_parents = @{$has_parents{$current_go}};
                    foreach my $new_parent (@new_parents) {
                        push @current_parents, $new_parent;
                    }
                }
            }



            ### print "\n"; ###
            ### print Dumper (\@family);    
            my @sorted_family = sort @family;
            ### print Dumper (\@sorted_family);
            my $last_member = '';
            my @new_family;
            foreach my $sorted_family (@sorted_family) {
                if ($sorted_family eq $last_member or exists ($gene2go_1{$x[0]}{$sorted_family})) {   ##### or exists ($gene2go{$gene}{$go})
                } else {
                    push @new_family, $sorted_family;   
                    $gene2go_1{$x[0]}{$sorted_family} = 1; ##### 
                }
                $last_member = $sorted_family;
            }
            ### print Dumper (\@new_family);
      
    

            foreach my $new_family (@new_family) {
                if ($go_counts1{$new_family}) {
                    ++$go_counts1{$new_family};
                } else {
                    $go_counts1{$new_family} = 1;
                }
                $go_terms{$new_family} = 1;
            }
        }
    }  
}
### print Dumper(\%go_counts1);
close B1FILE;





my $gene_count1 = scalar (keys %genes_1);

unless (open (B2FILE, $b2file)) {
    die "File $b2file not found.\n";
}

my %go_counts2;
my %genes_2;
my %gene2go_2; #####
my $sp2_gc = 0;

foreach my $in (<B2FILE>) {
    chomp $in;
    my @x = split (/\t/, $in);
    if ($x[$term_col] ne '.' and $x[0] !~ /^#/) {
        ++$sp2_gc;
        $genes_2{$x[0]} = 1;

        my @y = split (/;/, $x[$term_col]);
        foreach my $y (@y) {
            my $current_go = $y;    ### print "$current_go"; ###
            my @current_parents;
            my @family;
            push @family, $current_go;
            if ($has_parents{$current_go}) {  					##### working area
                @current_parents = @{$has_parents{$current_go}};
            } elsif ($current_go eq 'GO:0008150' or $current_go eq 'GO:0005575' or $current_go eq 'GO:0003674') {
            } else {
                print STDERR "No parents found for $current_go. Term may be obsolete or have an alternate id.\n"
            }
    
            while (@current_parents) {
                $current_go = shift @current_parents;
                push @family, $current_go;
                ### print " $current_go"; ###
        
                if ($has_parents{$current_go}) {
                    my @new_parents = @{$has_parents{$current_go}};
                    foreach my $new_parent (@new_parents) {
                        push @current_parents, $new_parent;
                    }
                }
            }



            ### print "\n"; ###
            ### print Dumper (\@family);    
            my @sorted_family = sort @family;
            ### print Dumper (\@sorted_family);
            my $last_member = '';
            my @new_family;
            foreach my $sorted_family (@sorted_family) {
                if ($sorted_family eq $last_member or exists ($gene2go_2{$x[0]}{$sorted_family})) { #####
                } else {
                    push @new_family, $sorted_family;
                    $gene2go_2{$x[0]}{$sorted_family} = 1; #####
                }
                $last_member = $sorted_family;
            }
            ### print Dumper (\@new_family);
      
    

            foreach my $new_family (@new_family) {
                if ($go_counts2{$new_family}) {
                    ++$go_counts2{$new_family};
                } else {
                    $go_counts2{$new_family} = 1;
                }
                $go_terms{$new_family} = 1;
            }
        }
    }  
}
### print Dumper(\%go_counts2);
close B2FILE;

my $gene_count2 = scalar (keys %genes_2);

##### Calculate Statistics #####

my @go_term_list = sort (keys %go_terms);

my @diff_go_terms;
foreach my $go_term (@go_term_list) {
    if ($go_counts1{$go_term}) {
    } else {
        $go_counts1{$go_term} = 0;
    }
    if ($go_counts2{$go_term}) {
    } else {
        $go_counts2{$go_term} = 0;
    }
    if ($go_term eq 'GO:0008150' or $go_term eq 'GO:0005575' or $go_term eq 'GO:0003674') {
    } elsif ($has_parents{$go_term}) {
        if (($go_counts1{$go_term} + $go_counts2{$go_term}) > 1) {
            push (@diff_go_terms, $go_term);
        }
    }
}

my %fisher_ps;

foreach my $go_term (@diff_go_terms) {
    #my $npp = $gene_count1 + $gene_count2;
    #my $np1 = $gene_count1;
    my $npp = $sp1_gc + $sp2_gc;
    my $np1 = $sp1_gc;
    my $n11 = $go_counts1{$go_term};
    my $n1p = $go_counts1{$go_term} + $go_counts2{$go_term};
    my $fisher_p;
    if ($right_flag) {
        $fisher_p = Stats::RightFisher::getRightFisher($n11,$n1p,$np1,$npp);
    } else {
        $fisher_p = Stats::TwoFisher::getTwoFisher($n11,$n1p,$np1,$npp);
    }
    if ($fisher_p > 1) {
        $fisher_p = 1;
    }
    if ($fisher_p < 0) {
        $fisher_p = 0;
    }
    
    $fisher_ps{$go_term} = $fisher_p;
}

#print Dumper(\%fisher_ps);
my $ps = \%fisher_ps;
print STDERR "Calculating q-values - this could take a while...\n";
my $res = eval 'qvalue($ps)';
if($@) {
    print STDERR $@;
}
my %qs = %$res;

##### Print Results #####

print "GO term\tsp1 count\tsp2 count\tfisher p\tq value\trel prop\tsig\tGO desc\n";

foreach my $go_term (@diff_go_terms) {
    my $rel_prop = sprintf("%.2f", ($go_counts1{$go_term}/($go_counts2{$go_term}+0.00001))*($sp2_gc/$sp1_gc)); # relative proportion
    print "$go_term\t$go_counts1{$go_term}\t$go_counts2{$go_term}\t$fisher_ps{$go_term}\t$qs{$go_term}\t$rel_prop\t";
    #print "$npp,$np1,$n11,$n1p\t";
    if ($qs{$go_term} < 0.05) {
        print "*";
    }
    print "\t$go_desc{$go_term}\n";
}

exit;