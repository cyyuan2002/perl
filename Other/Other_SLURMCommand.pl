#!/usr/bin/perl
use strict;
use Getopt::Long;
use Cwd;
use File::Temp qw/ tempfile tempdir /;
my %opts;
GetOptions(\%opts,"c:s","n:s","r:s","e:i","d:s","m:s","p:s","a:s","help");

if((!defined $opts{c})||(!defined $opts{n})){
	&Usage();
}

my $fileName;
my $pbsName;
my $ppNumber;
my $pbsFile;
my $directory;
my $emailnotify;
my $command;
my $nodeName;
my $addition;
my $memSize;

$command=$opts{c};
$pbsName=$opts{n};
$addition=$opts{a};
$directory=(defined $opts{r})?$opts{r}: getcwd();
$ppNumber=(defined $opts{p}) ? $opts{p} : 1;
$emailnotify=(defined $opts{e}) ? $opts{e} : 1;
$nodeName=$opts{d};
$memSize=$opts{m};


my @qsub;
(my $outfile, my $filename) = tempfile(DIR => $directory, UNLINK => 0, SUFFIX => '.sh');
#open(my $outfile,">temp.sh");
my $pbsoutFile=(defined $opts{o}) ? $opts{o} : "$pbsName.out";
my $pbserrFile=(defined $opts{e}) ? $opts{e} : "$pbsName.err";
print $outfile "\#!/bin/bash\n\#SBATCH --get-user-env\n\#SBATCH -J $pbsName\n\#SBATCH --cpus-per-task=$ppNumber\n";
print $outfile "$addition\n" if ($addition ne "");
if($nodeName ne ""){
	if ($nodeName eq 'm') {
		print $outfile "#SBATCH --partition=mmrl\n";
	}
	else{
		print $outfile "#SBATCH --partition=$nodeName\n";
	}
}

if ($memSize) {
	print $outfile "#SBATCH --mem-per-cpu=$memSize\n";
}

print $outfile "\#SBATCH -o $directory\/$pbsoutFile\n\#SBATCH -e $directory\/$pbserrFile\n";
if($emailnotify==1){
	print $outfile "\#SBATCH --mail-type=END\n\#SBATCH --mail-user=yuan.chen\@duke.edu\n";
}
print $outfile "cd $directory\n";
print $outfile "echo $command\necho Working directory is $directory\necho Running on host `hostname`\necho Time is `date`\n";
print $outfile "srun $command\n";
close $outfile;
`sbatch $filename`;
unlink $filename;
print "Job submitted!\n";
exit(1);

sub Usage(){
	print << "    Usage";

	Version	  : 1.0
	Date	  : 2014-11-5
	Author    : Yuan,Chen (MMRL,Duke University Medical Center)
        Email     : yuan.chen\@duke.edu

	Usage: $0 <options>

		-c     command, must be given (String)

		-n     job name, must be given (String)

		-r     job directory , default: current dir(String)

		-a     additional parameters for SLURM

		-p     the process number of the job, default value is 1

		-e     email notification when job finished, default 1

		-d     job node, default: mmrl
		
		-m     Memory size required

		-help  Show help

    Usage

	exit(0);
}
