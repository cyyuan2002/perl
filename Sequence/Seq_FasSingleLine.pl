#!/usr/bin/perl -w
use strict;

my $file = shift;
open(my $fh_file, $file);
my $seq="";
while (<$fh_file>) {
    chomp();
    if (/^>(\S+)/) {
        if ($seq ne "") {
            print "$seq\n";
        }
        
        print ">$1\n";
        $seq = "";
    }
    else{
        $seq .= $_;
    }
}
print "$seq\n";
close $fh_file;
exit(0)
