#!/usr/bin/env perl
#
# enrichment test script
# copied from ~cdesjard/plscripts/mol_evol_tools/pfam2fisherVII.pl
# questions email cdesjard@broadinstitute.org

use strict;
use warnings;
use Data::Dumper;
use Getopt::Long qw(:config no_ignore_case bundling pass_through);
use Statistics::Multtest qw(BH qvalue);
use FindBin;

use lib ("$FindBin::Bin/lib");

use Stats::WilsonInterval;
use Stats::RightFisher;
use Stats::TwoFisher;

my $usage = <<__EOUSAGE__;

################################################################################################ 
#
#  Conducts two-tailed Fisher's exact test with q-value FDR between files with ;-separated terms
#
#  Mandatory:
#
#  --annot_file_1 <string>         tab-delimited text file: ids in first column, ;-separated terms in second
#                                   ex.
#                                        locus_01	PF00001;PF00002
#                                        locus_02	PF00001
#                                        locus_03	PF00002;PF00004
#
#  --annot_file_2 <string>         tab-delimited text file: ids in first column, ;-separated terms in second
#                                   ex.
#                                        locus_04	PF00001
#                                        locus_05	PF00001;PF00002
#                                        locus_06	PF00003;PF00004
#
#  Optional:
#
#  --right_tailed                  conducts right-tailed Fisher test instead of two-tailed
#
#  --term_col                      tab-delimited column # where term appears (default = 1)
#
#  --include_empty                 include genes with "." terms
#
#  --BH                            Does q-value calculation with Benjamini-Hochberg method (default = Storey method)
#
#  Notes:
#
#    Ignores lines with terms coded as "." as appear in the cobra data repository files to denote empty values
#    Q-values take a while to compute - blame perl, not me!
#
################################################################################################

__EOUSAGE__

    ;

my $b1file;
my $b2file;
my $right_flag;
my $term_col = 1;
my $include_empty;
my $BH;

&GetOptions (
    'annot_file_1=s' => \$b1file,
    'annot_file_2=s' => \$b2file, 
    'term_col=i' => \$term_col,
    'right_tailed' => \$right_flag,
    'include_empty' => \$include_empty,
    'BH' => \$BH
    );

if (@ARGV) {
    die "Error, don't understand parameters: @ARGV";
}

unless ($b1file and $b2file) {
    die $usage;
}

##### Read in Files #####

unless (open (B1FILE, $b1file)) {
    die "File $b1file not found.\n";
}


my %pfam_counts1;
my %pfam_terms;
my $sp1_gene_count = 0;

foreach my $in (<B1FILE>) {
    chomp $in;
    my @x = split (/\t/, $in);    
    if ($x[$term_col] ne '.' or $include_empty) {
        ++$sp1_gene_count;
        my @y = split (/;/, $x[$term_col]);
        foreach my $y (@y) {
            if ($pfam_counts1{$y}) {
                ++$pfam_counts1{$y};
            } else {
                $pfam_counts1{$y} = 1;
                $pfam_terms{$y} = 1;
            }
        }
    }
}
close B1FILE;


unless (open (B2FILE, $b2file)) {
    die "File $b2file not found.\n";
}

my %pfam_counts2;
my $sp2_gene_count = 0;

foreach my $in (<B2FILE>) {
    chomp $in;
    my @x = split (/\t/, $in);
    if ($x[$term_col] ne '.' or $include_empty) {
        ++$sp2_gene_count;
        my @y = split (/;/, $x[$term_col]);
        foreach my $y (@y) {
            if ($pfam_counts2{$y}) {
                ++$pfam_counts2{$y};
            } else {
                $pfam_counts2{$y} = 1;
                $pfam_terms{$y} = 1;
            }
        }
    }
}
close B2FILE;

#print Dumper (\%pfam_desc);
#print Dumper (\ %pfam_counts1);

##### Calculate Statistics #####

my @pfam_term_list = sort (keys %pfam_terms);
my @diff_pfam_terms;

foreach my $pfam_term (@pfam_term_list) {   
    if ($pfam_counts1{$pfam_term}) {
    } else {
        $pfam_counts1{$pfam_term} = 0;
    }
    if ($pfam_counts2{$pfam_term}) {
    } else {
        $pfam_counts2{$pfam_term} = 0;
    }
    if (($pfam_counts1{$pfam_term} + $pfam_counts2{$pfam_term}) > 1) {
        push (@diff_pfam_terms, $pfam_term);
    }
}

#print Dumper(\@diff_pfam_terms);    
my %fisher_ps;
print "Pfam\tcount1\tcount2\tfisher p\tcorr p\trel prop\tsig\n";

foreach my $pfam_term (@diff_pfam_terms) {   
    my $npp = $sp1_gene_count + $sp2_gene_count;
    my $np1 = $sp1_gene_count;
    my $n11 = $pfam_counts1{$pfam_term};
    my $n1p = $pfam_counts1{$pfam_term} + $pfam_counts2{$pfam_term};

    my $fisher_p;
    if ($right_flag) {
        $fisher_p = Stats::RightFisher::getRightFisher($n11,$n1p,$np1,$npp);
    } else {
        $fisher_p = Stats::TwoFisher::getTwoFisher($n11,$n1p,$np1,$npp);
    }
    if ($fisher_p > 1) {
        $fisher_p = 1;
    }
    $fisher_ps{$pfam_term} = $fisher_p;
}

#print Dumper(\%fisher_ps);
my $ps = \%fisher_ps;
print STDERR "Calculating q-values - this could take a while...\n";
my $res;
if ($BH) {
	$res = eval 'BH($ps)';
} else {
	$res = eval 'qvalue($ps)';
}
if($@) {
    print STDERR $@;
}
my %qs = %$res;

##### Print Results #####

foreach my $pfam_term (@diff_pfam_terms) {
    my $rel_prop = sprintf("%.2f", ($pfam_counts1{$pfam_term}/($pfam_counts2{$pfam_term}+0.00001))*($sp2_gene_count/$sp1_gene_count)); # relative proportion
    print "$pfam_term\t$pfam_counts1{$pfam_term}\t$pfam_counts2{$pfam_term}\t$fisher_ps{$pfam_term}\t$qs{$pfam_term}\t$rel_prop\t";
    #print "$npp,$np1,$n11,$n1p\t";
    if ($qs{$pfam_term} < 0.05) {
        print "*";
    }
    print "\n";
}

exit;
