#!/usr/bin/env python
import sys
import re
FileList = ""

if len(sys.argv) < 2:
    raise ValueError("Usage:%s <RepeatMasker_Output>" %(sys.argv[0]))
else:
    FileList = sys.argv[1]

Fh_File = open(FileList,'r')
Fh_File_lines = Fh_File.readlines()
Fh_File_lines = Fh_File_lines[3:]
lastChrom = ""
lastStart = 0
lastEnd = 0
totallength = 0
for line in Fh_File_lines:
    stripped = line.strip()
    infors = re.findall(r"[\S']+",stripped)
    if lastChrom != infors[4]:
        if lastStart != 0:
            totallength += lastEnd-lastStart+1
            print ("%s\t%s\t%s" %(lastChrom,lastStart,lastEnd))

        lastChrom = infors[4]
        lastStart = int(infors[5])
        lastEnd = int(infors[6])
        
    else:
        if int(infors[5]) > lastEnd+1:
            print ("%s\t%s\t%s" %(lastChrom,lastStart,lastEnd))
            totallength += lastEnd-lastStart+1
            lastStart = int(infors[5])
            lastEnd = int(infors[6])
        else:
            if int(infors[6]) > lastEnd:
                lastEnd = int(infors[6])
            else:
                pass
            
totallength += lastEnd-lastStart+1
print ("%s\t%s\t%s" %(lastChrom,lastStart,lastEnd))
sys.stderr.write("Total length is: %s\n" %(totallength))


