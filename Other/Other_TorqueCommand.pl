#!/usr/bin/perl
use strict;
use Getopt::Long;
use Cwd;
my %opts;
GetOptions(\%opts,"c:s","n:s","r:s","e:s","p:s","q:s","a:s","help");

if((!defined $opts{c})||(!defined $opts{n})){
	Usage();
}

my $fileName;
my $pbsName;
my $ppNumber;
my $pbsFile;
my $directory;
my $emailnotify;
my $command;
my $nodeName;
my $addition;
my $queue;

$command=$opts{c};
$pbsName=$opts{n};
$addition=$opts{a};
$directory=(defined $opts{r})?$opts{r}: getcwd();
$ppNumber=(defined $opts{p}) ? $opts{p} : 1;
$emailnotify=(defined $opts{e}) ? $opts{e} : 0;
$queue=(defined $opts{q}) ? $opts{q} : "batch";

my @qsub;
open(my $outfile,">temp.PBS");
my $pbsoutFile=(defined $opts{o}) ? $opts{o} : "$pbsName.out";
my $pbserrFile=(defined $opts{e}) ? $opts{e} : "$pbsName.err";
print $outfile "\#!/bin/bash\n\#PBS -N $pbsName\n\#PBS -l ncpus=$ppNumber\n";
print $outfile "$addition\n" if ($addition ne "");
print $outfile "\#PBS -o $directory\/$pbsoutFile\n\#PBS -e $directory\/$pbserrFile\n";
print $outfile "\#PBS -q $queue\n";

if($emailnotify==1){
	print $outfile "\#PBS -m ae -M ychen\@aperiomics.com\n";
}
elsif ($emailnotify==2){
	print $outfile "\#PBS -m ae -M cstrope\@aperiomics.com\n";
}
print $outfile "cd $directory\n";
print $outfile "echo $command\necho Working directory is $directory\necho Running on host `hostname`\necho Time is `date`\n";
print $outfile "$command\n";
close $outfile;
`qsub temp.PBS`;
unlink "temp.PBS";
print "Job submitted!\n";
exit(1);

sub Usage(){
	print << "    Usage";

	Version	  : 1.0
	Date	  : 2015-11-03
	Author    : Yuan,Chen (Aperiomics)
        Email     : ychen\@Aperiomics.com

	Usage: $0 <options>

		-c     command, must be given (String)

		-n     name of the PBS job, must be given (String)

		-r     the directory of the PBS job , default: current dir(String)

		-a     additional parameters for PBS

		-p     number of processes for the job, default value is 1
		
		-q     job queue for submission, default "batch"

		-e     email notification when job finished, 0 None, 1 to Yuan, 2 to Cory (default 0)

		-help  Show help

    Usage

	exit(0);
}
