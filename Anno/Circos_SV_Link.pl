#!/usr/bin/perl
use strict;

my $isweight = 0;

if(@ARGV < 1){
	print "Usage:$0 <SV_File> [isWeighted:default 0]\n";
	exit(1);
}

if(@ARGV == 2){
	$isweight = 1;
}
my $file=shift;

my $count=0;

open(my $fh_file,"$file");
while(<$fh_file>){
    chomp();
    my @lines=split(/\t/,$_);
	my $end1;
	my $end2;
	if($isweight){
		$end1=$lines[1]+$lines[5];
		$end2=$lines[3]+$lines[5];
	}
	else{
		$end1=$lines[1]+1;
		$end2=$lines[3]+1;
	}
    print "tranloc$count $lines[0] $lines[1] $end1\n";
    print "tranloc$count $lines[2] $lines[3] $end2\n";
    $count++;
}
close $fh_file;

exit(0);