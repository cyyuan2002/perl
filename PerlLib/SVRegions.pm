package SVRegions;
use strict;


sub ReadRegionFiles {
    my ($Filename,$Colchr,$ColPosA,$ColPosB,$SpMode,$skipRow,$isSort,$isMerge)=@_;
    $skipRow ||= 0;
    $isSort ||= 1;
    $isMerge ||= 0;
    $SpMode ||= 0;
    my $LastChr;
    my %ChrRegions;
    my @Region;
    open(my $fh_filein,"$Filename");
    for(my $i=0;$i<=$skipRow;$i++){
        <$fh_filein>;
    }
    while (<$fh_filein>) {
        chomp();
        my @lines;
        @lines = split(/\t/,$_) if($SpMode == 0);
        @lines = split(/\s+/,$_) if($SpMode == 1);
        if ($LastChr ne $lines[$Colchr]) {
            if ($LastChr ne "") {
                my @tempArray=@Region;
                $ChrRegions{$LastChr}=\@tempArray;
            }
            @Region=();
            $LastChr=$lines[$Colchr];
        }
        my %pos;
        $pos{'s'}=$lines[$ColPosA];
        $pos{'e'}=$lines[$ColPosB];
        push(@Region,\%ChrRegions);
    }
    close $fh_filein;
    {
        my @tempArray=@Region;
        $ChrRegions{$LastChr}=\@tempArray;
    }
    if ($isSort == 1) {
        %ChrRegions = %{SortRegions(\%ChrRegions)};
    }
    if ($isMerge == 1){
        %ChrRegions = %{MergeRegion(\%ChrRegions)};
    }
    return \%ChrRegions;
}

sub SortRegions{
    my $refRegions=shift;
    my %ChrRegions=%{$refRegions};
    foreach my $chr(keys %ChrRegions){
        my @Regions = @{$ChrRegions{$chr}};
        my @sortedRegions;
        while (@Regions > 1) {
            my $minindex=0;
            for(my $i=1;$i<@Regions;$i++){
                if ($Regions[$i]->{'s'} < $Regions[$minindex]->{'s'} ) {
                    $minindex=$i;
                }
                
            }
            push(@sortedRegions,$Regions[$minindex]);
            splice(@Regions,$minindex,1);
        }
        $ChrRegions{$chr}=\@sortedRegions;
    }
    return \%ChrRegions;
}

sub MergeRegion{
    my $refRegions=shift;
    my %ChrRegions=%{$refRegions};
    foreach my $chr(keys %ChrRegions){
        my @Regions = @{$ChrRegions{$chr}};
        my $lastS = $Regions[0]->{'s'};
        my $lastE = $Regions[0]->{'e'};
        my @newRegions;
        for(my $i=1;$i<@Regions;$i++){
            if ($Regions[$i] -> {'s'} <= $lastE + 1) {
                $lastE = $Regions[$i] -> {'e'};
            }
            else{
                my %pos;
                $pos{'s'} = $lastS;
                $pos{'e'} = $lastE;
                push(@newRegions,\%pos);
                $lastS = $Regions[$i]->{'s'};
                $lastE = $Regions[$i]->{'e'};
            }
        }
        {
            my %pos;
            $pos{'s'} = $lastS;
            $pos{'e'} = $lastE;
            push(@newRegions,\%pos);
        }
        $ChrRegions{$chr} = \@newRegions;
    }
    return \%ChrRegions;
}

sub trim($)
{
    my $string = shift;
    $string =~ s/^\s+//;
    $string =~ s/\s+$//;
    return $string;
}

1;