#!/usr/bin/perl
use strict;
use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use File::Temp qw/ tempfile tempdir /;
my %opts;
GetOptions(\%opts,"i:s","r:s","e:i","d:s","m:s","p:s","a:s","help");

if(!defined $opts{i}){
	&Usage();
}

my $fileName;
my $pbsName;
my $ppNumber;
my $pbsFile;
my $directory;
my $emailnotify;
my $commandFile;
my $nodeName;
my $addition;
my $memSize;

my $scriptdir = dirname(abs_path($0));
my $SLURMcommand = $scriptdir."/Other_SLURMCommand.pl";

if (! -e $SLURMcommand) {
	die "Cannot find $SLURMcommand";
}

$commandFile=$opts{i};
$addition=$opts{a};
$directory=$opts{r};
$ppNumber=$opts{p};
$emailnotify=$opts{e};
$nodeName=$opts{d};
$memSize=$opts{m};

my $commandcount = 0;
open(my $fh, "$commandFile") or die "Cannot open file:$commandFile";
while (<$fh>) {
	chomp();
	$commandcount ++;
	$pbsName = "$commandFile.Job$commandcount ";
	my $command = "";
	$command = " -c \"$_\" -n $pbsName ";
	if ($directory ne "") {
		$command .= "-r $directory ";
	}
	if ($ppNumber ne "") {
		$command .= "-p $ppNumber ";
	}
	if ($addition ne "") {
		$command .= "-a $addition ";
	}
	if ($emailnotify ne "") {
		$command .= "-e $emailnotify ";
	}
	if ($nodeName ne "") {
		$command .= "-d $nodeName ";
	}
	if ($memSize ne "") {
		$command .= "-m $memSize ";
	}
	`perl $SLURMcommand $command`;
}
close($fh);


sub Usage(){
	print << "    Usage";

	Version	  : 1.0
	Date	  : 2014-11-5
	Author    : Yuan,Chen (MMRL,Duke University Medical Center)
        Email     : yuan.chen\@duke.edu

	Usage: $0 <options>

		-i     command file, must be given (String)

		-r     job directory , default: current dir(String)

		-a     additional parameters for SLURM

		-p     the process number of the job, default value is 1

		-e     email notification when job finished, default 1

		-d     job node, default: mmrl
		
		-m     Memory size required

		-help  Show help

    Usage

	exit(0);
}
