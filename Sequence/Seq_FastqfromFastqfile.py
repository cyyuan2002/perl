#!/usr/bin/env python
import sys
from Bio import SeqIO

if(len(sys.argv) < 3):
    print ("Usage:%s <Fastq_File> <SeqName_List>" %(sys.argv[0]))
    sys.exit(1)

SeqFile = sys.argv[1]
IdFile = sys.argv[2]

fh_File = open(IdFile,'r')
seqIds = {}
for line in fh_File:
    seqid = line.strip()
    seqIds[seqid] = 1
fh_File.close()

fh_SeqFile = open(SeqFile,'r')

for seq in SeqIO.parse(fh_SeqFile,"fastq-sanger"):
    if(seq.id in seqIds):
        print (seq.format('fastq'))
        seqIds[seq.id] = 0

fh_SeqFile.close()
