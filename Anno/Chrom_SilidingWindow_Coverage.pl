#!/usr/bin/perl

#===============================================================================
#
#         FILE:
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Yuan Chen
#      COMPANY: Division of Infectious Disease, DUMC
#      VERSION: 1.0
#      CREATED:
#     REVISION:
#===============================================================================

#-----Format of the input file
##chromA pos1 pos2 [exp_value]
#supercont2.1 19966 20490 1
#supercont2.1 46817 309447 1
#supercont2.1 102790 103523 1
#supercont2.1 108768 108831 2
#supercont2.1 113467 113658 4
#------------------------

#----Format of the chrom info
#chromA length
#supercont2.1	2291499
#supercont2.2	1621675
#supercont2.3	1575141
#supercont2.4	1084805
#supercont2.5	1814975
#--------------

use strict;
use Getopt::Long;

sub Usage{ #help subprogram

    print << "    Usage";

	Usage: $0 -i <input_file> -c <chrom_length> [options]

		-m  MODE: 1 - length caculation; 2 - expression value caculation; default 1

		-d  Delimiter: 1 - tab; 2 - spaces; default 1

		-w  Window size: default 2000

                -s  Step size: default 500

                -f  Output format: 1 - tab seperated; 2 - space seperated; default 1

                -l  Log transform of the output, 2 or 10; default off

    Usage

	exit(0);
};

sub Overlength{
    my ($start1,$end1,$start2,$end2) = @_;
    my ($start,$end);
    if($start1 <= $start2){
        $start = $start2;
    }
    else{
        $start = $start1;
    }

    if($end1 >= $end2){
        $end = $end2;
    }
    else{
        $end = $end1;
    }

    my $overlength = $end - $start + 1;
    return $overlength;
}

sub log10 {
    my $n = shift;
    return log($n)/log(10);
}

sub log2 {
    my $n =shift;
    return log($n)/log(2);
}

my %opts;
GetOptions(\%opts,"i=s","c=s","m:i","d:i","w:i","s:i","f:i","l:i");
if((!defined($opts{i})) || (!defined($opts{c})) ){
    Usage();
}

my $InputFile = $opts{'i'};
my $ChromFile = $opts{'c'};
my $Mode = $opts{'m'};
my $Delimiter = $opts{'d'};
my $WindowSize = $opts{'w'};
my $StepSize = $opts{'s'};
my $OutFormat = $opts{'f'};
my $LogTrans = $opts{'l'};

$Mode ||= 1;
$Delimiter ||= 1;
$WindowSize ||= 1000;
$StepSize ||= 500;
$OutFormat ||= 1;
$LogTrans ||= 0;

my %chroms;
my @chromName;
open(my $fh_chrominfo,$ChromFile) || die "Can't open file $ChromFile\n";
while(<$fh_chrominfo>){
    chomp();
    my @lines=split(/\t/,$_);
    $chroms{$lines[0]}=$lines[1];
    push(@chromName,$lines[0]);
}
close $fh_chrominfo;

my @SVrgns;
my $lastchrom;
my %ChromSVs;
open(my $fh_filein,$InputFile) || die "Can't open file $InputFile\n";
my $lastS=0;
my $lastE=0;
my $lastValue=0;
while(<$fh_filein>){
    chomp();
    my @lines;
    if ($Delimiter == 1) {
        @lines=split(/\t/,$_);
    }
    else{
        @lines=split(/\s+/,$_);
    }
    if($lastchrom ne $lines[0]){
        if($lastchrom ne ""){
            my %svcall;
            $svcall{'s'} = $lastS;
            $svcall{'e'} = $lastE;
            if($Mode == 2){
                $svcall{'v'} = $lastValue;
            }
            push (@SVrgns,\%svcall);
            my @tempsv = @SVrgns;
            $ChromSVs{$lastchrom} = \@tempsv;
            @SVrgns = ();
            $lastS=0;
            $lastE=0;
            $lastValue=0;
        }
        $lastchrom = $lines[0];
    }
    if($lines[1] <= $lastE){ ##overlap
        next if($lastE > $lines[2]);
        if($Mode == 2){
            $lastValue = (($lastE-$lastS+1)*$lastValue+($lines[2]-$lines[1]+1)
                          *$lines[3])/($lines[2]-$lastS+1);
        }
        $lastE=$lines[2];
    }
    else{
        my %svcall;
        $svcall{'s'} = $lastS;
        $svcall{'e'} = $lastE;
        if($Mode == 2){
            $svcall{'v'} = $lastValue;
            $lastValue=$lines[3];
        }
        push(@SVrgns,\%svcall) if($lastE!=0);
        $lastS=$lines[1];
        $lastE=$lines[2];
    }

}
{
    my %svcall;
    $svcall{'s'} = $lastS;
    $svcall{'e'} = $lastE;
    if($Mode == 2){
        $svcall{'v'} = $lastValue;
    }
    push (@SVrgns,\%svcall);
    my @tempsv = @SVrgns;
    $ChromSVs{$lastchrom} = \@tempsv;
}

foreach my $chr(@chromName){
    my $laststart=0;
    next if(!exists($ChromSVs{$chr}));
    for(my $i=1; ($i + $WindowSize - $StepSize) <= $chroms{$chr};$i+=$StepSize){
	my $SVcount=0;
	my @SVsites=@{$ChromSVs{$chr}};
        my $isstart=0;
	for(my $j=$laststart;$j<@SVsites;$j++){
            my %svinfo=%{$SVsites[$j]};
            if(($svinfo{'s'} <= $i && $svinfo{'e'} >= $i)
               || ($svinfo{'s'} <= $i+$WindowSize-1 && $svinfo{'e'} >= $i+$WindowSize-1)){
                if ($isstart==0) {
                    $laststart=$j;
                    $isstart=1;
                }
                if($Mode == 1){
                    my $start=$i;
                    my $end=$i+$WindowSize-1;
                    $SVcount += Overlength($svinfo{'s'},$svinfo{'e'},$start,$end);
                }
                else{
                    $SVcount += $svinfo{'v'};
                }
            }
            elsif($svinfo{'s'} >= $i && $svinfo{'e'} <= $i+$WindowSize-1){
                if ($isstart==0) {
                    $laststart=$j;
                    $isstart=1;
                }
                if($Mode == 1){
                    $SVcount += $svinfo{'e'} - $svinfo{'s'} + 1;
                }
                else{
                    $SVcount += $svinfo{'v'};
                }
            }
            last if ($svinfo{'s'} >$i+$WindowSize-1);
        }

        if($LogTrans != 0){
            if($LogTrans == 2){
                if($SVcount > 0){
                    $SVcount = log2($SVcount);
                }
            }
            elsif($LogTrans == 10){
                if($SVcount > 0){
                    $SVcount = log10($SVcount);
                }
            }
        }
        if ($OutFormat == 1) {
            print $chr,"\t",$i,"\t",$i+$StepSize-1,"\t",$SVcount,"\n";
	}
	else{
	    print $chr," ",$i," ",$i+$StepSize-1," ",$SVcount,"\n";
	}
    }
}

exit(0);
